const dataMakanan =[
{
	foto:"img/rendang.jpg",
	nama:'Rendang',
	desc:"Ingin tau resep dan cara pembuatannya? Klik tombol selengkapnya..."
},
{
	foto:"img/ayambakar.jpg",
	nama:'Ayam Bakar',
	desc:"Ingin tau resep dan cara pembuatannya? Klik tombol selengkapnya..."
},
{
	foto:"img/gambar3.jpg",
	nama:'Ayam pop',
	desc:"Ingin tau resep dan cara pembuatannya? Klik tombol selengkapnya..."
},
{
	foto:"img/gambar4.jpg",
	nama:'Dendeng batotok',
	desc:"Ingin tau resep dan cara pembuatannya? Klik tombol selengkapnya..."
},
{
	foto:"img/5gulaiotak.jpg",
	nama:'Gulai Otak',
	desc:"Ingin tau resep dan cara pembuatannya? Klik tombol selengkapnya..."
},
{
	foto:"img/gambar6.jpg",
	nama:'Gulai Kakap',
	desc:"Ingin tau resep dan cara pembuatannya? Klik tombol selengkapnya..."
},
{
	foto:"img/gambar7.jpg",
	nama:'Tunjang',
	desc:"Ingin tau resep dan cara pembuatannya? Klik tombol selengkapnya..."
},
{
	foto:"img/gambar8.jpg",
	nama:'Cincang',
	desc:"Ingin tau resep dan cara pembuatannya? Klik tombol selengkapnya..."
},
{
	foto:"img/gambar9.jpg",
	nama:'Telur Dadar',
	desc:"Ingin tau resep dan cara pembuatannya? Klik tombol selengkapnya..."
},
{
	foto:"img/gambarr10.jpg",
	nama:'Sayur Nangka',
	desc:"Ingin tau resep dan cara pembuatannya? Klik tombol selengkapnya..."
},
];

const callbackMap = (item, index)=>{
  const elmnt = document.querySelector('.daftar-makanan');

  elmnt.innerHTML += ` 
		  
		 		 <div class="col-md-3 mb-2">
            <div class="card">
              <img src="${item.foto}" class="card-img-top" alt="card" >
              <div class="card-body">
                <h4 class="card-title text-primary">${item.nama}</h4>
                <p class="card-text text-secondary">${item.desc}</p>
                <a href="#" class="btn btn-outline-primary">Selengkapnya</a>
              </div>
            </div>
          </div>
          
  `
}
dataMakanan.map(callbackMap);




const buttonElemnt = document.querySelector('.button-search');

buttonElemnt.addEventListener('click', ()=>{
  const hasilPencarian = dataMakanan.filter((item, index)=>{
                const inputElemnt    =document.querySelector('.input-keyword');
                const buttonElemnt   = document.querySelector('.button-search');
                const namaItem       = item.nama.toLowerCase();
                const keyword      = inputElemnt.value.toLowerCase();

                return namaItem.includes(keyword);
        })

  document.querySelector('.daftar-makanan').innerHTML ='';

  hasilPencarian.map(callbackMap);
});